import java.util.Arrays;

public class arrayList<T> {
    Object[] data;
    int size;
    public arrayList(){
        size = 0;
        data = new Object[0];
    }
    public arrayList(Object[] data){
        this.data = data;
        this.size = data.length;
    }

    public void add(T element){
        size++;
        Object[] dataNew = Arrays.copyOf(data, size);
        dataNew[size - 1] = element;
        data = dataNew;
    }

    public void remove(int num){ // удаление по индексу
        for(int i = num; i < size - 1; i++){
            data[i] = data[i + 1];
        }
        size--;
        data = Arrays.copyOf(data, size);
    }

    public void remove(T element){ // удаление по объекту
        boolean flag = false;
        for(int i = 0; i < size; i++){
            if(data[i].equals(element)){
                flag = true;
            }
            if(flag){
               data[i] = data[i + 1];
            }
        }
        size--;
        data = Arrays.copyOf(data, size);
    }

    public boolean contains(T element){
        for(Object iter : data){
            if(iter.equals(element)){
                return true;
            }
        }
        return false;
    }

    public int size(){
        return size;
    }

    public boolean isEmpty(){
        if(size == 0){
            return true;
        }
        return false;
    }

    public Object get(int index){
        return data[index];
    }

    public void clear(){
        data = new Object[0];
    }
}
